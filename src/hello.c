#include <stdio.h>
#include "hello.h"
#include "config.h"

void hello(void) {
#if defined PACKAGE_VERSION
  printf("hello %s\n", PACKAGE_VERSION);
#else
  printf("hello no version\n");
#endif
}
